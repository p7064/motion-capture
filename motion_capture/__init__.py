import cv2
import tempfile
from . import _version
import shutil
import mediapipe as mp
import numpy as np
__version__ = _version.get_versions()['version']

TEST_VAR = 1

#funkcja testowa
def f1():
    return 1


def potnij(path, start=0, end=None):  # tnie wideo na klatki, które zapisuje w folderze frames
    mp_pose = mp.solutions.pose
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    l = []  # tworzymy listę, w której będą listy - każda lista to jedna klatka, w każdej liście 33 słowniki z 4 współrzędnymi 33 punktów
    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)

        if end is None:
            fps = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fps
        else:
            duration = end

        i = 0

        cap.set(cv2.CAP_PROP_POS_MSEC, start)
        while (cap.isOpened() and cap.get(cv2.CAP_PROP_POS_MSEC) < duration):

            ret, frame = cap.read()
            if ret == False:
                break

            results = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5).process(frame)
            cv2.imwrite(frames + '/frame' + str(i) + '.jpg', frame)
            f = []  # chwilowa przechowywalnia landmarków w pierwotnej formie tj. x: ... y: ... z: ... visibility: ...
            mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                      landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            for i in range(33):
                f.append(results.pose_landmarks.landmark[i])
            keypoints = []  # tworzymy listę słowników odpowiadającą jednej klatce
            for data_point in f:
                keypoints.append({'X': data_point.x, 'Y': data_point.y, 'Z': data_point.z, 'Visibility': data_point.visibility, })
            l.append(keypoints)  # dodajemy listę słowników odpowiadającą jednej klatce do ogólnej listy z wszystkimi klatkami
            cv2_imshow(frame)

            # mp_drawing.plot_landmarks(results.pose_world_landmarks, mp_pose.POSE_CONNECTIONS)
            i += 1

        cap.release()
        cv2.destroyAllWindows()
        # print(l[0])
        # return l
        return 0


def rozmiar(path):
    mp_pose = mp.solutions.pose
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)
        while cap.isOpened() :
            ret, frame = cap.read()
            if ret == False:
                break
            height, width, layers = frame.shape
    return height, width
#input: mp4, ilość klatek na sekundę, czas: od do (domyślnie od początku do końca (w seknudach))
#output: lista z pozycjami lanmarków i zapisany filmik z naniesionymi landmarkami
def dodaj_landmark(path, fps, start = 0, end = None):
    mp_pose = mp.solutions.pose
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    height, width = rozmiar(path)
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('output.mp4', fourcc, fps, (width,height))
    l = []  # tworzymy listę, w której będą listy - każda lista to jedna klatka, w każdej liście 33 słowniki z 4 współrzędnymi 33 punktów
    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)
        if end is None:
            fp = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fp
        else:
            duration = end
        duration = duration * 1000  # zamiana na ms
        strat = start * 1000
        d = cap.get(cv2.CAP_PROP_FPS) // fps
        i = 0
        cap.set(cv2.CAP_PROP_POS_MSEC, start)
        while (cap.isOpened() and cap.get(cv2.CAP_PROP_POS_MSEC) < duration):
            i+=1
            ret, frame = cap.read()
            if ret == False:
                break
            if i%d == 0:
                results = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5).process(frame)
                f = []  # chwilowa przechowywalnia landmarków w pierwotnej formie tj. x: ... y: ... z: ... visibility: ...
                mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                      landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

                out.write(frame)  # dodajemy listę słowników odpowiadającą jednej klatce do ogólnej listy z wszystkimi klatkami
                if results.pose_landmarks is not None :  #sprawdzamy czy klatka ma landmarki
                    for j in range(33):
                        f.append(results.pose_landmarks.landmark[j])
                    keypoints = []  # tworzymy listę słowników odpowiadającą jednej klatce
                    for data_point in f:
                        keypoints.append({'X': data_point.x, 'Y': data_point.y, 'Z': data_point.z, 'Visibility': data_point.visibility, })
                    l.append(keypoints)
                else: #jak klatka jest bez landmarków to do listu dodajemy 0
                    l.append(0)
        out.release()
        cap.release()
        cv2.destroyAllWindows()
        return l