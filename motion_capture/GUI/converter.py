# tutaj bedzie gui z converterem z mp4 do pliku anim
from tkinter import *
from tkinter.filedialog import askopenfile
from tkinter import ttk
import tkinter as tk
from tkinterdnd2 import *
from PIL import ImageTk, Image
from tkinter import filedialog as fd
import tkinterdnd2 as dnd
import motion_capture.Scripts
from motion_capture.Scripts import sample
import os

window = Tk()
window.title("anim converter Motion Capture")
window.geometry("710x450")
window.configure(bg="#212121")


def open_file():
    file = askopenfile(mode='r', filetypes=[
        ('Video Files', ["*.mp4"])])
    if file is not None:
        global filename
        filename = file.name
        listbox.insert("end", filename)
        files_conv = [('anim Files', '*.anim')]
        file_conv = tk.filedialog.asksaveasfile(filetypes=files_conv, defaultextension="*.anim")
        sample.sample_anim(vid=file_conv.name, path=filename)


def close_win():
    window.destroy()


def backClick():
    close_win()
    from motion_capture.GUI import menu


def path_listbox(event):
    files = [('anim Files', '*.anim')]
    file = tk.filedialog.asksaveasfile(filetypes=files, defaultextension="*.anim")
    listbox.insert("end", event.data)
    sample.sample_anim(vid=file.name, path=event.data)


def save():
    files = [('anim Files', '*.anim')]
    file = tk.filedialog.asksaveasfile(filetypes=files, defaultextension="*.anim")


lbl1 = Label(window, text="Converter", bg="#212121",
             fg="white", font="arial 30 bold")
lbl1.config(anchor=CENTER)
lbl1.pack()

lbl2 = Label(window, text="convert you .mp4 to .anim", bg="#212121",
             fg="#34FFC8", font="arial 15 bold")
lbl2.config(anchor=CENTER)
lbl2.pack()

text = Label(window, text='drag it here', bg="#212121", fg='white', font='arial 10 bold')
text.config(anchor=CENTER)
text.pack()

listbox = Listbox(
    window,
    width=50,
    height=10,
    selectmode=SINGLE,
    bg='#212121',

)
listbox.pack(side=TOP)
listbox.drop_target_register(DND_FILES)
listbox.dnd_bind('<<Drop>>', path_listbox)

text2 = Label(window, text='or', bg="#212121", fg='white', font='arial 10 bold')
text2.config(anchor=CENTER)
text2.pack()

opn_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "opn_file2.png"))
openbtn = Button(window, text='open file', image=opn_img, command=lambda: [open_file()], background='#212121', borderwidth=0)
openbtn.pack(side=TOP, pady=3)

back_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "arrow.png"))
myButton = Button(window, text="back", image=back_img, command=backClick, background='#212121', borderwidth=0)
myButton.pack(side=BOTTOM, pady=2)

# save_img = PhotoImage(file=r"save2.png")
# btn = Button(window, text='Save', image=save_img, command=lambda: save(), background='#212121', borderwidth=0)
# btn.pack(side=BOTTOM, pady=15)

window.mainloop()
