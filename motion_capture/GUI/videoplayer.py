from tkinter import *
from tkinter.filedialog import askopenfile
from tkVideoPlayer import TkinterVideo
from motion_capture import rozmiar
from motion_capture import dodaj_landmark
from tkinter import ttk
import tkinter as tk
import time
import os


window = Tk()
window.title("Video Player Motion Capture")
window.geometry("700x600")
window.configure(bg="#212121")

#progress bar
def progress(opt):
    full = 100
    load = 0
    while (load < full):
        if opt == 0:
            speed = 1
            time.sleep(0.05)
            pb['value'] += (speed / full) * 100
            load += speed
            percent.set('dodawanie landmarków ' + str(int((load / full) * 100)) + "%")
            window.update_idletasks()
        else:
            speed = 2
            time.sleep(0.05)
            pb['value'] += (speed / full) * 100
            load += speed
            percent.set('wczytywanie filmu ' + str(int((load / full) * 100)) + "%")
            window.update_idletasks()

def stop():
    pb.stop()
    # value_label['text'] = update_progress_label()

def close_win():
    window.destroy()


def backClick():
    close_win()
    from motion_capture.GUI import menu


def open_file():
    img_temp1 = PhotoImage(file=os.path.join(os.path.dirname(__file__), "loading.png"))
    openbtn.configure(image=img_temp1)
    openbtn.image = img_temp1
    file = askopenfile(mode='r', filetypes=[
        ('Video Files', ["*.mp4"])])
    if file is not None:
        global filename
        filename = file.name
        global videoplayer
        progress(opt=0)
        dodaj_landmark(filename,12)
        videoplayer = TkinterVideo(master=window, scaled=True) #pre_load=False
        videoplayer.load(r"{}".format('output.mp4'))
        videoplayer.pack(expand=True, fill="both")
        progress(opt=1)
        videoplayer.play()
        img_temp2 = PhotoImage(file=os.path.join(os.path.dirname(__file__), "done.png"))
        openbtn.configure(image=img_temp2)
        openbtn.image = img_temp2


def playAgain():
    print(filename)
    videoplayer.play()


def StopVideo():
    print(filename)
    videoplayer.stop()


def PauseVideo():
    print(filename)
    videoplayer.pause()


#progress bar
def update_progress_label():
    return f"Loading video: {pb['value']}%"

percent = StringVar()
pb = ttk.Progressbar(
    window,
    orient='horizontal',
    mode='determinate',
    length=280,
    )

# # center
lbl1 = Label(window, text="Videoplayer", bg="#212121",
             fg="white", font="arial 30 bold")
lbl1.config(anchor=CENTER)
lbl1.pack()

opn_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "opn_file2.png"))
openbtn = Button(window, text='open', image=opn_img, command=lambda: [open_file()], background='#212121', borderwidth=0)
# openbtn_text.set("open .mp4 file")
openbtn.pack(side=TOP, pady=10)

play_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "play.png"))
playbtn = Button(window, text='Play Video', image=play_img, command=lambda: playAgain(), background='#212121', borderwidth=0)
playbtn.pack(side=TOP, pady=3)

# stopbtn = Button(window, text='Stop Video', command=lambda: StopVideo())
# stopbtn.pack(side=TOP, padx=4)

pause_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "pause.png"))
pausebtn = Button(window, text='Pause Video', image=pause_img, command=lambda: PauseVideo(), background='#212121', borderwidth=0)
pausebtn.pack(side=TOP, pady=3)

back_img = PhotoImage(file=os.path.join(os.path.dirname(__file__), "arrow.png"))
myButton = Button(window, text="back", image=back_img, command=backClick, background='#212121', borderwidth=0)
myButton.pack(side=BOTTOM, pady=6)

pb.pack(side=BOTTOM)
percentLabel = Label(window,textvariable=percent, bg='#212121', fg='white', font='arial 10 bold').pack(side=BOTTOM)

window.mainloop()