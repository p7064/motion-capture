from tkinter import *
from tkinter.filedialog import askopenfile
from tkVideoPlayer import TkinterVideo
from motion_capture import dodaj_landmark
import tkinter as tk
from tkinter import ttk
import time
import os

window = Tk()
window.title("Motion Capture")
window.geometry("650x450")
window.configure(bg="#212121")


def video_open():
    from motion_capture.GUI import videoplayer


def converter():
    close_win()
    from motion_capture.GUI import converter


def close_win():
    window.destroy()


def open_and_close():
    close_win()
    video_open()


lbl1 = Label(window, text="Welcome to Motion Capture", bg="#212121", fg="white", font="arial 35 bold")
lbl1.config(anchor=CENTER)
lbl1.pack()


photo1 = PhotoImage(file=os.path.join(os.path.dirname(__file__), "button1.png"))
photo2 = PhotoImage(file=os.path.join(os.path.dirname(__file__), "button2.png"))

conv_button = Button(window, text='.anim converter', image=photo2, background='#212121', command=lambda: converter(), borderwidth=0)
conv_button.pack(side=TOP, pady=25, padx=10)

v_button = Button(window, text='VideoPlayer', image=photo1, background='#212121', command=lambda: open_and_close(), borderwidth=0)
#foreground='#dff7f7', background='grey', height=5, width=20, font="Helvetica 15 bold"
v_button.pack(side=TOP, pady=10, padx=10)


window.mainloop()
