# from motion_capture import potnij
import cv2
import tempfile
import mediapipe as mp
import motion_capture.Scripts
import os

# from motion_capture import dodaj_landmark
mp_pose = mp.solutions.pose
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles


def rozmiar(path):

    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)
        while cap.isOpened() :
            ret, frame = cap.read()
            if ret == False:
                break
            height, width, layers = frame.shape
    return height, width
#input: mp4, ilość klatek na sekundę, czas: od do (domyślnie od początku do końca (w seknudach))
#output: lista z pozycjami lanmarków i zapisany filmik z naniesionymi landmarkami


def potnij_z_zapisywaniem(path, start=0, end=None):  # tnie wideo na klatki, które zapisuje w folderze frames
    mp_pose = mp.solutions.pose
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    l = []  # tworzymy listę, w której będą listy - każda lista to jedna klatka, w każdej liście 33 słowniki z 4 współrzędnymi 33 punktów
    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)

        if end is None:
            fps = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fps
        else:
            duration = end

        i = 0

        cap.set(cv2.CAP_PROP_POS_MSEC, start)
        while (cap.isOpened() and cap.get(cv2.CAP_PROP_POS_MSEC) < duration):

            ret, frame = cap.read()
            if ret == False:
                break

            results = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5).process(frame)
            cv2.imwrite(frames + '/frame' + str(i) + '.jpg', frame)
            f = []  # chwilowa przechowywalnia landmarków w pierwotnej formie tj. x: ... y: ... z: ... visibility: ...
            mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                      landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            for i in range(33):
                f.append(results.pose_landmarks.landmark[i])
            keypoints = []  # tworzymy listę słowników odpowiadającą jednej klatce
            for data_point in f:
                keypoints.append(
                    {'X': data_point.x, 'Y': data_point.y, 'Z': data_point.z, 'Visibility': data_point.visibility, })
            l.append(
                keypoints)  # dodajemy listę słowników odpowiadającą jednej klatce do ogólnej listy z wszystkimi klatkami
            # cv2_imshow(frame)

            # mp_drawing.plot_landmarks(results.pose_world_landmarks, mp_pose.POSE_CONNECTIONS)
            i += 1

        cap.release()
        cv2.destroyAllWindows()

        # return l
        return l


def STAREdodaj_landmark(path, fps, height, width, start=0, end=None):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('output.mp4', fourcc, fps, (height, width))
    l = []  # tworzymy listę, w której będą listy - każda lista to jedna klatka, w każdej liście 33 słowniki z 4 współrzędnymi 33 punktów
    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)
        if end is None:
            fp = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fp
        else:
            duration = end
        duration = duration * 1000  # zamiana na ms
        strat = start * 1000
        d = 60 // fps
        i = 0
        cap.set(cv2.CAP_PROP_POS_MSEC, start)
        while (cap.isOpened() and cap.get(cv2.CAP_PROP_POS_MSEC) < duration):
            i += 1
            ret, frame = cap.read()
            if ret == False:
                break
            if i % d == 0:
                results = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5).process(frame)
                f = []  # chwilowa przechowywalnia landmarków w pierwotnej formie tj. x: ... y: ... z: ... visibility: ...
                mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                          landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
                out.write(
                    frame)  # dodajemy listę słowników odpowiadającą jednej klatce do ogólnej listy z wszystkimi klatkami
                if results.pose_landmarks is not None:  # sprawdzamy czy klatka ma landmarki
                    for j in range(33):
                        f.append(results.pose_landmarks.landmark[j])
                    keypoints = []  # tworzymy listę słowników odpowiadającą jednej klatce
                    for data_point in f:
                        keypoints.append({'X': data_point.x, 'Y': data_point.y, 'Z': data_point.z,
                                          'Visibility': data_point.visibility, })
                    l.append(keypoints)
                else:  # jak klatka jest bez landmarków to do listu dodajemy 0
                    l.append(0)
        out.release()
        cap.release()
        #cv2.destroyAllWindows()
        return l

def dodaj_landmark(path, fps, start = 0, end = None):
    mp_pose = mp.solutions.pose
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles

    height, width = rozmiar(path)
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter('output.mp4', fourcc, fps, (width,height))
    l = []  # tworzymy listę, w której będą listy - każda lista to jedna klatka, w każdej liście 33 słowniki z 4 współrzędnymi 33 punktów
    with tempfile.TemporaryDirectory() as frames:
        cap = cv2.VideoCapture(path)
        if end is None:
            fp = cap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            duration = frame_count / fp
        else:
            duration = end
        duration = duration * 1000  # zamiana na ms
        strat = start * 1000
        d = cap.get(cv2.CAP_PROP_FPS) // fps
        i = 0
        cap.set(cv2.CAP_PROP_POS_MSEC, start)
        while (cap.isOpened() and cap.get(cv2.CAP_PROP_POS_MSEC) < duration):
            i+=1
            ret, frame = cap.read()
            if ret == False:
                break
            if i%d == 0:
                results = mp_pose.Pose(min_detection_confidence=0.5, min_tracking_confidence=0.5).process(frame)
                f = []  # chwilowa przechowywalnia landmarków w pierwotnej formie tj. x: ... y: ... z: ... visibility: ...
                mp_drawing.draw_landmarks(frame, results.pose_landmarks, mp_pose.POSE_CONNECTIONS,
                                      landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

                out.write(frame)  # dodajemy listę słowników odpowiadającą jednej klatce do ogólnej listy z wszystkimi klatkami
                if results.pose_landmarks is not None :  #sprawdzamy czy klatka ma landmarki
                    for j in range(33):
                        f.append(results.pose_landmarks.landmark[j])
                    keypoints = []  # tworzymy listę słowników odpowiadającą jednej klatce
                    for data_point in f:
                        keypoints.append({'X': data_point.x, 'Y': data_point.y, 'Z': data_point.z, 'Visibility': data_point.visibility, })
                    l.append(keypoints)
                else: #jak klatka jest bez landmarków to do listu dodajemy 0
                    l.append(0)
        out.release()
        cap.release()
        #cv2.destroyAllWindows()
        return l


def sample_anim(x=None, y=None, z=None, vid='sample.anim', path="sample.mp4"):  # pisze plik anim na podstawie wideo podanego w vid

    height, width = rozmiar(path)
    print(height, width)

    def xyz(x, y, z):
        return ("{x: " + str(x) + ", y: " + str(y) + ", z: " + str(z) + "}")


    def addSv3(t, x, y, z):
        f.write(szablonSv3.format(time=t, value=xyz(x, y, z), inSlope=xyz(0, 0, 0), outSlope=xyz(0, 0, 0),
                                  inWeight=xyz(0.33333334, 0.33333334, 0.33333334),
                                  outWeight=xyz(0.33333334, 0.33333334, 0.33333334)))

    def konwertuj_liste_klatek_na_listy_kazdego_landmrku(lista_klatek, fps, visibilityThreshold):
        lista_landmarkow = [[] for i in range(33)]

        dt = 1 / fps

        print(lista_klatek)
        for frame, i in enumerate(lista_klatek):
            for c, v in enumerate(i):
                if v["Visibility"]>=visibilityThreshold:
                    lista_landmarkow[c].append([frame * dt, v["X"], -v["Y"], v["Z"]])

        return lista_landmarkow

    with open(vid, "w+") as f, open(os.path.join(os.path.dirname(__file__), 'curveend.txt'), 'r') as cend, \
            open(os.path.join(os.path.dirname(__file__),'curvestart.txt'), 'r') as cstart, \
            open(os.path.join(os.path.dirname(__file__),'filestart.txt'), 'r') as fstart, \
            open(os.path.join(os.path.dirname(__file__),'fileend.txt'), 'r') as fend,\
            open(os.path.join(os.path.dirname(__file__),'serializedversion3.txt'),'r') as sv3:
        start = fstart.read()
        f.write(start[0:211] + " " + vid[0:-5] + start[211:])

        szablonSv3 = sv3.read()
        szablonCstart=cstart.read()
        szablonCend=cend.read()
        szablonFend=fend.read()

        fps = 15
        visibilityThreshold = 0.80  # poniżej tego Visibility igonrujemy landmarki
        lista_landmarkow = konwertuj_liste_klatek_na_listy_kazdego_landmrku(dodaj_landmark(path, fps, 0),
                                                               fps, visibilityThreshold)
        print(lista_landmarkow)

        najdalszyCzas=0

        for c,i in enumerate(lista_landmarkow):
            f.write(szablonCstart)

            for j in i:
                print("dodaję sv3")
                addSv3(j[0], j[1], j[2], j[3])

                if j[0]>najdalszyCzas:
                    najdalszyCzas=j[0]

            f.write(szablonCend.format(landmarkNumber=c))


        #print(szablonFend.format(fileID="{fileID: 0}", stopTime="5", zero=xyz(0,0,0)))
        f.write(szablonFend.format(fileID="{fileID: 0}", stopTime=najdalszyCzas, zero=xyz(0,0,0), loopTime=najdalszyCzas))