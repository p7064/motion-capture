import cv2
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

def position(frame,point):
    '''
    :param frame: do której klatki
    :param point: który landmark
    :return: historia zmian położenia podanego landmarku
    '''
    if point > 32:
        print("Punkt musi mieć numer od 0 do 32")
    l = []
    cap = cv2.VideoCapture(0)
    with mp_pose.Pose(min_detection_confidence=0.5,min_tracking_confidence=0.5) as pose:
        while cap.isOpened():
            success, image = cap.read()
            if not success:
                print("Ignorowanie pustego/złego obrazu")
                continue
            image.flags.writeable = False
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            results = pose.process(image)

            image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            mp_drawing.draw_landmarks(image,results.pose_landmarks,mp_pose.POSE_CONNECTIONS,
                landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
            if cv2.waitKey(5) & 0xFF == 27:
                break
            if results.pose_landmarks is not None:
                l.append(results.pose_landmarks.landmark[point])
            if len(l) == (frame):
                #mp_drawing.plot_landmarks(results.pose_landmarks, mp_pose.POSE_CONNECTIONS)
                break
    keypoints = []
    for data_point in l:
        keypoints.append({'X': data_point.x,'Y': data_point.y,'Z': data_point.z,'Visibility': data_point.visibility,})
    return keypoints
