"""Basic tests to test testing system."""
import motion_capture

def test_import():
    """Package testing stub."""
    assert motion_capture.TEST_VAR == 1


def test_function():
    assert motion_capture.f1() == 1

def test_cutting():
    '''
    w folderze znajduje się plik film_do_testowania.mp4 który ma 60 fps
    Funkcja do cięcia w ustawieniu potnij(***,7000,8000) ma 61 klatek
    Sprawdzamy ilość wyjsciowych klatek/plików w folderze frames w .jpg
    '''
    assert motion_capture.potnij("film_do_testowania.mp4", 7000, 8000) == 0