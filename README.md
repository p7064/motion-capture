# Motion Capture

Motion Capture
Nagrywacz Motion Capture:

• konwertowanie plików z rozszerzeniem .mp4 do formatu .anim
• tworzenie i prezentowanie demo w postaci filmu
• generowanie modelu człowieczego w programie Unity

https://assetstore.unity.com/packages/3d/characters/robots/space-robot-kyle-4696
model z projektu https://github.com/vrm-c/UniVRM/releases


# Poradnik Git

1. Dodawanie projektu na swój komputer:
*   Kopiujemy link https
*   odpalamy terminal/git bash
*   wpisujemy: git clone https://gitlab.com/p7064/motion-capture.git
*   aby wejść do projektu wpisujemy "cd motion-capture"

```
git clone https://gitlab.com/p7064/motion-capture.git
cd motion-capture
```

2. Dodawanie swoich funkcji/fragmentu kodu

* Jeśli dodajesz nowy plik (np. ".py") to dodajemy na ten moment to do folderu motion_capture (pod initem)
* Najlepiej otworzyć projekt w pycharmie
* Po wprowadzeniu zmian robisz:
```
git checkout -b (Tutaj wpisujemy nazwe brancha naszego dev/2_...)
git add . (dodaje wszystkie zmiany co zrobilismy)
git commit -m (Tutaj obowiązkowo jakie zmiany zrobilismy i #numer_problemu)
```
* ważne żeby pracować i dodawać zmiany na innym branchu niz master (można sprawdzić na którym branchu jesteś przez git checkout)
* Jeśli na naszym komputerze wszystko działa wrzucamy wszystko:

```
git push
```


https://gitlab.com/p7064


## Installation

```
pip install motion-capture
```

## License
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>

    Niniejszy program jest wolnym oprogramowaniem; możesz go
    rozprowadzać dalej i/lub modyfikować na warunkach Powszechnej
    Licencji Publicznej GNU, wydanej przez Fundację Wolnego
    Oprogramowania - według wersji 3 tej Licencji lub (według twojego
    wyboru) którejś z późniejszych wersji.

    Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on
    użyteczny - jednak BEZ JAKIEJKOLWIEK GWARANCJI, nawet domyślnej
    gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH
    ZASTOSOWAŃ. W celu uzyskania bliższych informacji sięgnij do     Powszechnej Licencji Publicznej GNU.

    Z pewnością wraz z niniejszym programem otrzymałeś też egzemplarz
    Powszechnej Licencji Publicznej GNU (GNU General Public License);
    jeśli nie - zobacz <http://www.gnu.org/licenses/>.


