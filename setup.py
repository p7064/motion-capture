# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
from setuptools import setup, find_packages

import versioneer


INSTALL_REQUIREMENTS = {
    'setup': [
    ],
    'install': [
        'numpy==1.19.3',
        'opencv-contrib-python==4.5.5.64',
        'opencv-python==4.5.5.64',
        'mediapipe==0.8.9.1',
        'tkinterdnd2==0.3.0',
        'tkvideoplayer==1.3.0',
        'protobuf==3.19.4',

    ],
}

if __name__ == '__main__':
    setup(
        name='motion_capture',
        version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass(),
        description='example gitlab CI project',
        zip_safe=False,
        author='Magdalena Biczyk, Kornel Natoński, Marcin Syc',
        author_email='k.natonski@student.uw.edu.pl',
        license='Other/Proprietary License',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Natural Language :: English',
            'Topic :: Scientific/Engineering',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.9',
        ],
        keywords='motion capture mediapipe',
        packages=find_packages(include=['motion_capture']),
        include_package_data=True,
        setup_requires=INSTALL_REQUIREMENTS['setup'],
        install_requires=INSTALL_REQUIREMENTS['install'],
        extras_require=INSTALL_REQUIREMENTS,
        entry_points={
            'console_scripts': ['motion-capture = motion_capture.GUI:menu'],
        },
        ext_modules=[],
    )
